/**
 * * Copyright &copy; 2015-2020 <a href="https://gitee.com/JeeHuangBingGui/JeeSpring">JeeSpring</a> All rights reserved..
 */
package com.jeespring.modules.test.dao.onetomany;

import com.jeespring.common.persistence.InterfaceBaseDao;
import org.apache.ibatis.annotations.Mapper;
import com.jeespring.modules.test.entity.onetomany.TestDataMain;

/**
 * 票务代理DAO接口
 * @author liugf
 * @version 2016-01-15
 */
@Mapper
public interface TestDataMainDao extends InterfaceBaseDao<TestDataMain> {
	
}