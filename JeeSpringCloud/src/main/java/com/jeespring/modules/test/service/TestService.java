/**
 * Copyright &copy; 2012-2016 <a href="https://git.oschina.net/guanshijiehnan/JeeRTD">JeeSite</a> All rights reserved.
 */
package com.jeespring.modules.test.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeespring.modules.test.dao.TestDao;
import com.jeespring.modules.test.entity.Test;
import com.jeespring.common.service.AbstractBaseService;

/**
 * 测试Service
 * @author 黄炳桂 516821420@qq.com
 * @version 2013-10-17
 */
@Service
@Transactional(readOnly = true)
public class TestService extends AbstractBaseService<TestDao, Test> {

}
