<?xml version="1.0" encoding="utf-8"?>
<template>
	<name>service</name>
	<filePath>src/main/java/${packageName}/${moduleName}/service/${subModuleName}</filePath>
	<fileName>${ClassName}Service.java</fileName>
	<content><![CDATA[
/**
 * * Copyright &copy; 2015-2020 <a href="https://gitee.com/JeeHuangBingGui/JeeSpring">JeeSpring</a> All rights reserved..
 */
package ${packageName}.${moduleName}.service<#if subModuleName != "">.${subModuleName}</#if>;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeespring.common.persistence.Page;
import com.jeespring.common.service.AbstractBaseService;
import com.jeespring.common.utils.StringUtils;
import com.alibaba.fastjson.JSON;
import com.jeespring.common.redis.RedisUtils;
import com.jeespring.common.security.MD5Tools;
import ${packageName}.${moduleName}.entity<#if subModuleName != "">.${subModuleName}</#if>.${ClassName};
import ${packageName}.${moduleName}.dao<#if subModuleName != "">.${subModuleName}</#if>.${ClassName}Dao;
<#list table.childList as c>
import ${packageName}.${moduleName}.entity<#if subModuleName != "">.${subModuleName}</#if>.${c.className?cap_first};
import ${packageName}.${moduleName}.dao<#if subModuleName != "">.${subModuleName}</#if>.${c.className?cap_first}Dao;
</#list>

/**
 * ${functionName}Service
 * @author ${functionAuthor}
 * @version ${functionVersion}
 */
@Service
@Transactional(readOnly = true)
public class ${ClassName}Service extends AbstractBaseService<${ClassName}Dao, ${ClassName}> {

	/**
	 * redis caches
	 */
	@Autowired
	private RedisUtils redisUtils;

	<#list table.childList as c>
	@Autowired
	private ${c.className?cap_first}Dao ${c.className?uncap_first}Dao;
	</#list>
	
	public ${ClassName} get(String id) {
		//获取数据库数据
		${ClassName} ${className} = super.get(id);
		if(${className} ==null) return new ${ClassName}();
		<#list table.childList as c>
		${className}.set${c.className?cap_first}List(${c.className?uncap_first}Dao.findList(new ${c.className?cap_first}(${className})));
		</#list>
		return ${className};
	}

	public ${ClassName} getCache(String id) {
		//获取缓存数据
		${ClassName} ${className}=(${ClassName})redisUtils.get(RedisUtils.getIdKey(${ClassName}Service.class.getName(),id));
		if( ${className}!=null) return  ${className};
		//获取数据库数据
		${className} = super.get(id);
		if(${className} ==null) return new ${ClassName}();
		<#list table.childList as c>
		${className}.set${c.className?cap_first}List(${c.className?uncap_first}Dao.findList(new ${c.className?cap_first}(${className})));
		</#list>
		//设置缓存数据
		redisUtils.set(RedisUtils.getIdKey(${ClassName}Service.class.getName(),id),${className});
		return ${className};
	}

	public ${ClassName} findListFirst(${ClassName} ${className}) {
		//获取数据库数据
		List<${ClassName}> ${className}List=super.findList(${className});
		if(${className}List.size()>0) ${className}=${className}List.get(0);
		return ${className};
	}

	public ${ClassName} findListFirstCache(${ClassName} ${className}) {
		//获取缓存数据
		String findListFirstKey = RedisUtils.getFindListFirstKey(${ClassName}Service.class.getName(),JSON.toJSONString(${className}));
		${ClassName} ${className}Redis=(${ClassName})redisUtils.get(findListFirstKey);
		if(${className}Redis!=null) return ${className}Redis;
		//获取数据库数据
		List<${ClassName}> ${className}List=super.findList(${className});
		if(${className}List.size()>0) ${className}=${className}List.get(0);
		else ${className}=new ${ClassName}();
		//设置缓存数据
		redisUtils.set(findListFirstKey,${className});
		return ${className};
	}

	public List<${ClassName}> findList(${ClassName} ${className}) {
		//获取缓存数据
		String findListKey = RedisUtils.getFindListKey(${ClassName}Service.class.getName(),JSON.toJSONString(${className}));
		List<${ClassName}> ${className}List=(List<${ClassName}>)redisUtils.get(findListKey);
		if(${className}List!=null) return ${className}List;
		//获取数据库数据
		${className}List=super.findList(${className});
		//设置缓存数据
		redisUtils.set(findListKey,${className}List);
		return ${className}List;
	}

	public List<${ClassName}> findListCache(${ClassName} ${className}) {
		//获取缓存数据
		String findListKey = RedisUtils.getFindListKey(${ClassName}Service.class.getName(),JSON.toJSONString(${className}));
		List<${ClassName}> ${className}List=(List<${ClassName}>)redisUtils.get(findListKey);
		if(${className}List!=null) return ${className}List;
		//获取数据库数据
		${className}List=super.findList(${className});
		//设置缓存数据
		redisUtils.set(findListKey,${className}List);
		return ${className}List;
	}

	public Page<${ClassName}> findPage(Page<${ClassName}> page, ${ClassName} ${className}) {
		//获取数据库数据
		Page<${ClassName}> pageReuslt=super.findPage(page, ${className});
		return pageReuslt;
	}

	public Page<${ClassName}> findPageCache(Page<${ClassName}> page, ${ClassName} ${className}) {
		//获取缓存数据
		String findPageKey =  RedisUtils.getFindPageKey(${ClassName}Service.class.getName(),JSON.toJSONString(page)+JSON.toJSONString(${className}));
		Page<${ClassName}> pageReuslt=(Page<${ClassName}>)redisUtils.get(findPageKey);
		if(pageReuslt!=null) return pageReuslt;
		//获取数据库数据
		pageReuslt=super.findPage(page, ${className});
		//设置缓存数据
		redisUtils.set(findPageKey,pageReuslt);
		return pageReuslt;
	}

	@Transactional(readOnly = false)
	public void save(${ClassName} ${className}) {
		//保存数据库记录
		super.save(${className});
	<#list table.childList as c>
		for (${c.className?cap_first} ${c.className?uncap_first} : ${className}.get${c.className?cap_first}List()){
			if (${c.className?uncap_first}.getId() == null){
				continue;
			}
			if (${c.className?cap_first}.DEL_FLAG_NORMAL.equals(${c.className?uncap_first}.getDelFlag())){
				if (StringUtils.isBlank(${c.className?uncap_first}.getId())){
					<#if c.parentExists>
						<#list c.columnList as cc>
							<#if c.parentTableFk == cc.name>
					${c.className?uncap_first}.set${cc.simpleJavaField?cap_first}(${className});
							</#if>
						</#list>
					</#if>
					${c.className?uncap_first}.preInsert();
					${c.className?uncap_first}Dao.insert(${c.className?uncap_first});
				}else{
					${c.className?uncap_first}.preUpdate();
					${c.className?uncap_first}Dao.update(${c.className?uncap_first});
				}
			}else{
				${c.className?uncap_first}Dao.delete(${c.className?uncap_first});
			}
		}
	</#list>
		//设置清除缓存数据
		redisUtils.remove(RedisUtils.getIdKey(${ClassName}Service.class.getName(),${className}.getId()));
		//清除列表和页面缓存数据
		redisUtils.removePattern(RedisUtils.getFindListKeyPattern(${ClassName}Service.class.getName()));
		redisUtils.removePattern(RedisUtils.getFinPageKeyPattern(${ClassName}Service.class.getName()));
	}
	
	@Transactional(readOnly = false)
	public void delete(${ClassName} ${className}) {
		//清除记录缓存数据
		redisUtils.remove(RedisUtils.getIdKey(${ClassName}Service.class.getName(),${className}.getId()));
		//删除数据库记录
		super.delete(${className});
	<#list table.childList as c>
		${c.className?uncap_first}Dao.delete(new ${c.className?cap_first}(${className}));
	</#list>
		//清除列表和页面缓存数据
		redisUtils.removePattern(RedisUtils.getFindListKeyPattern(${ClassName}Service.class.getName()));
		redisUtils.removePattern(RedisUtils.getFinPageKeyPattern(${ClassName}Service.class.getName()));
	}

	@Transactional(readOnly = false)
	public void deleteByLogic(${ClassName} ${className}) {
		//清除记录缓存数据
		redisUtils.remove(RedisUtils.getIdKey(${ClassName}Service.class.getName(),${className}.getId()));
		//逻辑删除数据库记录
		super.deleteByLogic(${className});
	<#list table.childList as c>
		${c.className?uncap_first}Dao.deleteByLogic(new ${c.className?cap_first}(${className}));
	</#list>
		//清除列表和页面缓存数据
		redisUtils.removePattern(RedisUtils.getFindListKeyPattern(${ClassName}Service.class.getName()));
		redisUtils.removePattern(RedisUtils.getFinPageKeyPattern(${ClassName}Service.class.getName()));
	}

}]]>
	</content>
</template>