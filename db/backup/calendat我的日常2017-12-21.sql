DROP TABLE IF EXISTS `calendar`;
CREATE TABLE `calendar` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `title` varchar(64) default NULL COMMENT '事件标题',
  `starttime` varchar(64) character set latin1 default NULL COMMENT '事件开始时间',
  `endtime` varchar(64) character set latin1 default NULL COMMENT '事件结束时间',
  `allday` varchar(64) character set latin1 default NULL COMMENT '是否为全天时间',
  `color` varchar(64) character set latin1 default NULL COMMENT '时间的背景色',
  `userid` varchar(64) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日历';

INSERT INTO `calendar` (`id`,`title`,`starttime`,`endtime`,`allday`,`color`,`userid`) VALUES 
 ('343ed3c7486f41298a9bc9df342b8d27','sss','2016-04-27','','1','#f30','1'),
 ('74b3da86093c4d8eb3a95093d6c8b212','333','2016-05-04','','1','#06c','1'),
 ('ac9d77bb842a4e7f9afb55d26e8fd51d','上午开会','2016-04-05 080000','2016-04-05 120000','0','#f30',NULL),
 ('d468d4f2982e409280c1d328d9f3d1c0','一起吃饭','2016-04-21','','1','#360',NULL),
 ('ed8112f26f764301b73ee6671806b6e1','去看电影','2016-04-23','','1','#06c',NULL),
 ('fe29fbbdc50e4d27b266b15fb90f0d41','早上要开会','2016-04-07','2016-04-07','1','#06c',NULL);
 
 INSERT INTO `sys_menu` (`id`,`parent_id`,`parent_ids`,`name`,`sort`,`href`,`target`,`icon`,`is_show`,`permission`,`create_by`,`create_date`,`update_by`,`update_date`,`remarks`,`del_flag`) VALUES 
('0aa3712414d049a6a24e8bcddeae509a','27','0,1,27,','我的日程','100','/iim/myCalendar','','','1','','1','2016-04-21 21:52:06','1','2016-04-21 21:52:06','','0');